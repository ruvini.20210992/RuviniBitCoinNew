package com.example;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BitCoinController {
	
	@GetMapping("/getappname")
	public String getAppName() {
		return "Name : BitCoin Application Ruvini - 9";
	}

	@GetMapping("/getappversion")
	public String getAppVersion() {
		return "Version : 9.0.0";
	}
}
